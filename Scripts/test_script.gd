extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var torque:float =0.0;
var x:float = 0.0;
var y:float = 0.0;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _unhandled_input(event):
	if event is InputEventJoypadButton:
		if event.pressed == true:
			if event.button_index == JOY_L:
				torque = torque+ 100;
			if event.button_index == JOY_R:
				torque = torque-100;
		else:
			if event.button_index == JOY_L:
				torque = torque-100;
			if event.button_index == JOY_R:
				torque = torque+100;
	if event is InputEventJoypadMotion:
		if event.get_axis() == JOY_AXIS_0:
			x = event.get_axis_value();
		if event.get_axis() == JOY_AXIS_1:
			y = event.get_axis_value();

func _physics_process(delta):
	if abs(x)>0 or abs(y)>0:
		apply_central_impulse(Vector2(x,y))	
	if abs(torque)>0:
		apply_torque_impulse(torque)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
