class_name CHUD

extends Control

var m_FuelLevel: float setget SetFuelLevel
var m_MaterialLevel: float setget SetMaterialLevel
var m_CriticalThreshold : float = 10;

var m_FuelCritical : bool setget SetFuelCritical
var m_MaterialCritical : bool setget SetMaterialCritical

signal sig_FuelDepleted;
signal sig_MaterialDepleted;
signal sig_ResumeGame;

# Called when the node enters the scene tree for the first time.
func _ready():
	HideGameOver();
	HideInGameMenu();

func _process(delta):
	pass;
	#CheckFuel();
	#CheckMaterial();

func ShowGameOver(value=0):
	if value >0:
		$GameOver/Label.set_text("Level Completed")
		for a in range(0,value):
			var starPath = String("GameOver/Container/Star")+String(a+1);
			var star = get_node(NodePath(starPath));
			if star:
				get_node(NodePath(starPath)).set_visible(true);
	$GameOver.set_visible(true);
	
func HideGameOver():
	for a in range(1,4):
		get_node(NodePath("GameOver/Container/Star"+String(a))).set_visible(false);
	$GameOver.set_visible(false);
	
func CheckFuel():
	if m_FuelLevel < m_CriticalThreshold:
		if !m_FuelCritical:
			SetFuelCritical(true);
	else:
		if m_FuelCritical:
			SetFuelCritical(false);

func SetFuelCritical(critical:bool):
	if critical  != m_FuelCritical:
		m_FuelCritical = critical;
		var tween = get_node("Container/FuelLevel/Tween");
		if m_FuelCritical:
			tween.interpolate_property($Container/FuelLevel, "modulate:a",0, 1, 1, Tween.TRANS_LINEAR);
			tween.start();
		else:
			tween.stop_all();
			$Container/FuelLevel.modulate.a = 1;

func SetFuelLevel(value:float):
	m_FuelLevel = value;
	$Container/FuelLevel.value = m_FuelLevel;
	if m_FuelLevel <=0 :
		emit_signal("sig_FuelDepleted");
	CheckFuel();

func CheckMaterial():
	if m_MaterialLevel < m_CriticalThreshold:
		if !m_MaterialCritical:
			SetMaterialCritical(true);
	else:
		if m_MaterialCritical:
			SetMaterialCritical(false);

func SetMaterialCritical(critical:bool):
	if critical  != m_MaterialCritical:
		m_MaterialCritical = critical;
		var tween = get_node("Container/MaterialLevel/Tween");
		if m_MaterialCritical:
			tween.interpolate_property($Container/MaterialLevel, "modulate:a",0, 1, 1, Tween.TRANS_LINEAR);
			tween.start();
		else:
			tween.stop_all();
			$Container/MaterialLevel.modulate.a = 1;

func SetMaterialLevel(value:float):
	m_MaterialLevel = value;
	$Container/MaterialLevel.value = m_MaterialLevel 
	if m_MaterialLevel <=0 :
		emit_signal("sig_MaterialDepleted");
	CheckMaterial();

func RegisterTask(task):
	$Tasks.add_child(task);

func SetPoints(points):
	$PointsLabel.text = "POINTS: "+String(points);

func ShowInGameMenu():
	$InGameMenu.set_visible(true);
	$InGameMenu/Resume.grab_focus();

func HideInGameMenu():
	$InGameMenu.set_visible(false);

func _on_Resume_pressed():
	emit_signal("sig_ResumeGame")
	HideInGameMenu();

func _on_BackToMenu_pressed():
	get_tree().set_pause(false);
	GlobalLoader.goto_scene("res://Interfaces/MissionsMap.tscn")
