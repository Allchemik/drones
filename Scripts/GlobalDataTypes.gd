extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum DebrisTypeName{red,green,blue,cyan,magenta,yellow};


class DebrisType:
	var m_type:int;
	var m_color:Color;
	func Init(type:int,color:Color):
		m_type = type;
		m_color = color;


var DebrisTypes:Array;

var colors = [Color.red,Color.green,Color.blue,Color.cyan,Color.magenta,Color.yellow];


# Called when the node enters the scene tree for the first time.
func _ready():
	for a in range(0,colors.size()):
		var debrisType = DebrisType.new();
		debrisType.Init(a,colors[a]);
		DebrisTypes.append(debrisType);

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
