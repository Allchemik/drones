class_name CTransmutator
extends Node2D


onready var debrisPrefab = preload("res://obejcts/Debris.tscn")
onready var Docks = [$Assembled/Body/Docks/Dock_Left/DockCorrect,
	$Assembled/Body/Docks/Dock_Right/DockCorrect,
	$Assembled/Body/Docks/Dock_Top/DockCorrect,
	$Assembled/Body/Docks/Dock_Bottom/DockCorrect
	];
var DockedObjects = [null,null,null,null];

var m_TransmutatorOutputType=0;
var m_IsAssembled = false;
var m_picker = null;

signal Connecting();

func _ready():
	Disassemble();
	for a in range(0,Docks.size()):
		SetDockActive(a,false);
	UpdateOutputTypePresentation();

func GetDissasembledBody()->RigidBody2D:
	return $Disassembled/Body as RigidBody2D;

func Disassemble():
	m_IsAssembled = false;
	$Disassembled.set_visible(true);
	$Disassembled/Body/Shape.set_disabled(false)
	$Assembled.set_visible(false);
	$Assembled/Body/Shape.set_deferred("Disabled",true);

func Assemble():
	m_IsAssembled = true;
	$Disassembled.set_visible(false);
	$Disassembled/Body/Shape.set_disabled(true)
	$Assembled.set_visible(true);
	$Assembled/Body/.set_global_position($Disassembled/Body/.get_global_position());
	if $Assembled/Body/Shape.is_disabled():
		$Assembled/Body/Shape.set_disabled(false);
	#$Assembled/Body/Shape.set_deferred("Disabled",false);

func UseObject(drone):
	if (!m_IsAssembled):
		Assemble()
		emit_signal("Connecting",self);
		if is_connected("Connecting",m_picker,"Drop"):
			disconnect("Connecting",m_picker,"Drop");
	elif (m_IsAssembled):
		if(CanSpawnObject()):
			var whereToSpawn = findFirstEmptyDock();
			if(whereToSpawn==null):
				return;
			if(SpawnObjectOfType(m_TransmutatorOutputType,whereToSpawn)):
				destroyObjectsInDocks();
		else:
			var newType = m_TransmutatorOutputType +1;
			if(newType>=GlobalDataTypes.DebrisTypes.size()):
				newType = newType-GlobalDataTypes.DebrisTypes.size();
			m_TransmutatorOutputType = newType;
			UpdateOutputTypePresentation();

func UpdateOutputTypePresentation():
	$Assembled/Body/OutputType.set_color(GlobalDataTypes.DebrisTypes[m_TransmutatorOutputType].m_color);

func SpawnObjectOfType(debrisType,whereToSpawn:Node)->bool:
	var targetParent = get_parent().find_node("Debris");
	if targetParent:
		var newDebris = debrisPrefab.instance();
		targetParent.add_child(newDebris);
		newDebris.set_global_rotation(deg2rad(45));
		newDebris.set_global_position(whereToSpawn.get_global_position())
		var newDebrisObject = newDebris.find_node("RigidBody2D");
		if newDebrisObject as CDebris:
			newDebrisObject.initializeDebrisOfType(debrisType);
			return true;
	return false;

func destroyObjectsInDocks():
	for a in DockedObjects:
		if(a != null):
			a.queue_free();

func Pickup(picker:Node):
	if(!m_IsAssembled):
		if(m_picker!= null):
			return;
		m_picker = picker;
		if(m_picker.has_method("Drop")):
			connect("Connecting",m_picker,"Drop");

func Drop(value):
	if(!m_IsAssembled):
		if m_picker!=null:
			if is_connected("Connecting",m_picker,"Drop"):
				disconnect("Connecting",m_picker,"Drop");
				m_picker=null;

func CanGrab()->bool:
	return !m_IsAssembled;

func CountEmptyDocks()->int:
	var result =0;
	for a in DockedObjects:
		if(a==null):
			result =result+1;
	return result;

func findFirstEmptyDock()->Node:
	for a in  range(0,DockedObjects.size()):
		if(DockedObjects[a]==null):
			return Docks[a];
	return null;

func CanSpawnObject()->bool:
	return CountEmptyDocks()==1;

func SetDockActive(dock,active:bool,body:Node=null):
	if active:
		Docks[dock].set_color(Color.black);
		if(DockedObjects[dock]==null):
			DockedObjects[dock]=body
	else:
		Docks[dock].set_color(Color.white);
		DockedObjects[dock]=body
		

func Destroy():
	queue_free();

func _on_Dock_Left_Area_body_entered(body):
	if body.is_in_group("Debris"):
		SetDockActive(0,true,body)

func _on_Dock_Left_Area_body_exited(body):
	if body.is_in_group("Debris"):
		SetDockActive(0,false)

func _on_Dock_Right_Area_body_entered(body):
	if body.is_in_group("Debris"):
		SetDockActive(1,true,body)

func _on_Dock_Right_Area_body_exited(body):
	if body.is_in_group("Debris"):
		SetDockActive(1,false)

func _on_Dock_Bottom_Area_body_entered(body):
	if body.is_in_group("Debris"):
		SetDockActive(3,true,body)

func _on_Dock_Bottom_Area_body_exited(body):
	if body.is_in_group("Debris"):
		SetDockActive(3,false)

func _on_Dock_Top_Area_body_entered(body):
	if body.is_in_group("Debris"):
		SetDockActive(2,true,body)

func _on_Dock_Top_Area_body_exited(body):
	if body.is_in_group("Debris"):
		SetDockActive(2,false)
