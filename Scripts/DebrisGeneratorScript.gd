class_name CDebrisGenerator
extends Node

onready var DebrisPrefab = preload("res://obejcts/Debris.tscn")

var m_running = true;

export(Array,GlobalDataTypes.DebrisTypeName)var DebrisForLevel;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func spawnDebris():
	var width = $Start/CollisionShape2D.get_shape().get_extents().x;
	var height = $Start/CollisionShape2D.get_shape().get_extents().y;
	var x = $Start/CollisionShape2D.get_global_position().x;
	var y = $Start/CollisionShape2D.get_global_position().y;
	var startx = x - width;
	var starty = y - height;
	var endx = x + width;
	var endy = y + height;
	var startPosition = Vector2(rand_range(startx,endx),rand_range(starty,endy))
	var newDebris = DebrisPrefab.instance()
	self.add_child(newDebris);
	newDebris.set_global_position(startPosition)
	var newDebrisObject = newDebris.find_node("RigidBody2D");
	if newDebrisObject as CDebris:
		newDebrisObject.InitializeFreeFloat(Vector2(-10,15));
		var debrisType = int(rand_range(0,DebrisForLevel.size()))
		newDebrisObject.initializeDebrisOfType(DebrisForLevel[debrisType]);

func spawnTutorialDebris(startPosition,debrisType=-1)->Node2D:
	var newDebris = DebrisPrefab.instance()
	self.add_child(newDebris);
	newDebris.set_global_position(startPosition)
	var newDebrisObject = newDebris.find_node("RigidBody2D");
	if newDebrisObject as CDebris:
		if debrisType <0:
			debrisType = int(rand_range(0,DebrisForLevel.size()))
		newDebrisObject.initializeDebrisOfType(DebrisForLevel[debrisType]);
	return newDebris;


func _on_GameArea_body_exited(body):
	if body.is_in_group("Debris"):
		body.queue_free()


func _on_Timer_timeout():
	if m_running:
		spawnDebris()
