class_name CExplosive
extends Node2D

export(int) var explosivCountdown = 5;
onready var m_timeleft = explosivCountdown; 

var m_connectedAsteroidObject:Node = null;
var m_asteroidConnected:bool = false;
var m_exploded:bool = false;
var m_picker:Node;

signal Exploding(value);
signal Connecting();
signal PickedUp(value);
signal AsteroidInRange(value);

#Tutorial stuff
func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");

func CanGrab()->bool:
	return true;

# Called when the node enters the scene tree for the first time.
func _ready():
	$RigidBody2D/CountDownLabel.set_visible(false);

func Pickup(picker:Node):
	if(m_picker!= null):
		return;
	m_picker = picker;
	if(m_picker.has_method("Drop")):
		connect("Connecting",m_picker,"Drop");
	emit_signal("PickedUp",self);

func Drop(value):
	if m_picker!=null:
		if is_connected("Connecting",m_picker,"Drop"):
			disconnect("Connecting",m_picker,"Drop");
			m_picker=null;

func UseObject(object):
	if !m_asteroidConnected:
		connectAsteroid();
	else:
		StartCountdown();

func connectAsteroid():
	if(!m_asteroidConnected and m_connectedAsteroidObject != null):
		var pin = $RigidBody2D/AsteroidConnectionPoint; 
		pin.set_node_b(m_connectedAsteroidObject.get_path());
		m_asteroidConnected = true;
		emit_signal("Connecting",self);

func StartCountdown():
	if (m_asteroidConnected):
		$RigidBody2D/CountDownLabel.set_visible(true);
		UpdateCountdownLabel();
		$Timer.start();
		connect("Exploding",m_connectedAsteroidObject,"DestroyAsteroid");

func Explode():
	$RigidBody2D/ExplosionParticle.set_emitting(true);
	$RigidBody2D/CountDownLabel.set_visible(false);
	$RigidBody2D/Sprite.set_visible(false);
	m_exploded = true;
	emit_signal("Exploding",self);
	if( m_connectedAsteroidObject):
		if(is_connected("Exploding",m_connectedAsteroidObject,"DestroyAsteroid")):
			disconnect("Exploding",m_connectedAsteroidObject,"DestroyAsteroid");

func ExplodeEnd():
	queue_free();

func UpdateCountdownLabel():
	$RigidBody2D/CountDownLabel.text = String(m_timeleft);
	
func _on_Timer_timeout():
	if(m_exploded):
		ExplodeEnd();
	else:
		m_timeleft= m_timeleft - $Timer.get_wait_time();
		$Timer.start();
		UpdateCountdownLabel();
		if (m_timeleft<=0):
			Explode();

func _on_Area2D_body_entered(body):
	if body.is_in_group("Asteroids"):
		m_connectedAsteroidObject = body;
		emit_signal("AsteroidInRange",self);

func _on_Area2D_body_exited(body):
	if m_connectedAsteroidObject != null:
		if body == m_connectedAsteroidObject:
			m_connectedAsteroidObject = null;

func Destroy():
	queue_free()
