class_name CGlobal
extends Node

class Profile:
	var savePath = "user://save.save"
	var CollectedStars:int =0;
	var Levels:Array;
	var Tutorial = true;
	class LevelInfo:
		var Locked:bool = true;
		var StarsRequired:int = 0;
		var StarsCollected:int = 0;
		func toDictionary()->Dictionary:
			var data = {
			'Locked':Locked,
			'StarsRequired':StarsRequired,
			'StarsCollected':StarsCollected
			};
			return data;
	
	func toDictionary()->Dictionary:
		var levelsArray = Array();
		for a in Levels:
			levelsArray.append(a.toDictionary());
		var data = {
			"CollectedStars": CollectedStars,
			"Tutorial":Tutorial,
			"StoredLevels":Levels.size(),
			"levelsArray":levelsArray
			};
		return data;

	func SaveProfile():
		var save_file = File.new()
		save_file.open(savePath, File.WRITE)
		var dictionaryToSave = toDictionary();
		save_file.store_string(JSON.print(dictionaryToSave))
		save_file.close()

	func LoadProfile()->bool:
		Levels.clear();
		var save_file = File.new();
		if not save_file.file_exists(savePath):
			return false;
		save_file.open(savePath, File.READ);
		var text = save_file.get_as_text();
		var parsedData =  JSON.parse(text);
		if parsedData.error != OK:
			print("error parsing save data");
			return false;
		var savedata = parsedData.result;
		CollectedStars = savedata["CollectedStars"];
		Tutorial = savedata["Tutorial"];
		var storedLevelsAmount = savedata["StoredLevels"];
		var storedLevelsArray = savedata["levelsArray"];
		for a in storedLevelsArray:
			var newLevelInfo = LevelInfo.new();
			newLevelInfo.Locked = a["Locked"];
			newLevelInfo.StarsRequired = a["StarsRequired"];
			newLevelInfo.StarsCollected = a["StarsCollected"];
			Levels.append(newLevelInfo);
		save_file.close()
		return true;

	func InitializeProfile():
		Levels.clear();
		CollectedStars = 0;
		var config_file = File.new();
		var configPath = "res://levels.config"
		if not config_file.file_exists(configPath):
			print("ERROR!");
		config_file.open(configPath, File.READ)
		var levels = JSON.parse(config_file.get_as_text())
		for a in levels.result:
			var newLevelInfo = LevelInfo.new();
			newLevelInfo.Locked = true;
			newLevelInfo.StarsRequired= a["StarsRequired"];
			newLevelInfo.StarsCollected =0;
			Levels.append(newLevelInfo);

	func FinishLevel(LevelNo:int, Points:int):
		if Levels[LevelNo].StarsCollected < Points:
			var pointsToAdd = Points - Levels[LevelNo].StarsCollected;
			Levels[LevelNo].StarsCollected = Points;
			CollectedStars = CollectedStars+pointsToAdd;
			SaveProfile();


var loader;
var wait_frames;
var time_max = 100;
var current_scene = null;
var lScene ;
var loadede_elements = 0;

var MainProfile;

func _ready():
	var root = get_tree().get_root();
	current_scene = root.get_child(root.get_child_count()-1);
	if(!MainProfile):
		MainProfile = Profile.new();

func GetMainProfile()->Profile:
	if(!MainProfile):
		MainProfile = Profile.new();
	return MainProfile;

func goto_scene(path):
	loader = ResourceLoader.load_interactive(path)
	if loader == null:
		return;
	set_process(true);
	current_scene.queue_free();
	lScene = get_tree().get_root().get_node("loading_scene");
	if(lScene):
		lScene.play("loading");
	wait_frames = 1;
	
	#call_deferred("_deferred_goto_scene",path);
	
func _process(time):
	if loader == null:
		set_process(false);
		return;
	if wait_frames>0:
		wait_frames -= 1;
		return;
		
	var t = OS.get_ticks_msec();
	while OS.get_ticks_msec() < t + time_max:
		var err = loader.poll()
		
		if err == ERR_FILE_EOF:
			var resource = loader.get_resource();
			loader = null;
			set_new_scene(resource);
			break;
		elif err == OK:
			loadede_elements+=1;
			update_progress()
		else:
			loader = null;
			break;


func update_progress():
	var progress = float(loader.get_stage())/loader.get_stage_count()
#	get_node("progress").set_progress(progress)
	if(lScene):
		var length = lScene.get_current_animation().length();
		lScene.seek(progress*length,true);
	

func set_new_scene(scene_resource):
	current_scene = scene_resource.instance()
	get_node("/root").add_child(current_scene);
	loadede_elements =0;
	
