class_name CLevelCotroller
extends Node

onready var TaskPrefab = preload("res://obejcts/TaskPresentationTemplate.tscn")

enum GameState {Initial ,Start, Running, Paused, End}
enum TaskType {PointsTask,ExplosiveTask,TransmutatorTask,MaterialTask}

export (int) var LevelNumber
export (NodePath) var HUDPath
export (NodePath) var TutorialGUIPath
export (Array,TaskType) var TasksForThisLevel
export (Array,Array,GlobalDataTypes.DebrisTypeName) var DebrisForTask
export (int) var StarCondition1
export (int) var StarCondition2
export (int) var StarCondition3

var InitialFuel:float = 100;
var InitialMaterial:float = 100;

var m_CurrentLevelState= GameState.Initial;
var m_LevelPoints:int =0;

onready var m_CurrentFuelLevel : float = InitialFuel;
onready var m_CurrentMaterialLevel : float = InitialMaterial;

var m_ConsumedFuel :float =0;
var m_ConsumedMaterial:float =0;

var m_MotherShip;

var m_Tasks:Array

onready var HUD:CHUD = get_node(HUDPath)
onready var TutorialGUI:CTutorial = get_node(TutorialGUIPath)

#tutorials
export(Array, CTutorial.requestedActions) var m_TutorialSteps;
var m_CurrentTutorial = 0;
var m_CurrentTutorialObject:CTutorial.CTutorialObject;




# Called when the node enters the scene tree for the first time.
func _ready():
	m_CurrentLevelState = GameState.Initial;
	HUD.SetFuelLevel(InitialFuel);
	HUD.SetMaterialLevel(InitialMaterial)
	HUD.SetPoints(m_LevelPoints);
	ExecuteNextTutorial();
	get_tree().set_pause(true);
	for a in range(0,TasksForThisLevel.size()):
		AddNewTask(a);

#tutorial stuff
func ExecuteNextTutorial():
	if not TutorialGUI:
		GlobalLoader.GetMainProfile().Tutorial = false;
		return;
	if GlobalLoader.GetMainProfile().Tutorial:
		if m_CurrentTutorial<m_TutorialSteps.size():
			m_CurrentTutorialObject = TutorialGUI.MakeToutorialObject(m_TutorialSteps[m_CurrentTutorial])
			m_CurrentTutorialObject.RequisterTutorialCallback(self);
			m_CurrentTutorialObject.StartTutorial();
		else:
			GlobalLoader.GetMainProfile().Tutorial = false;
			TutorialGUI.HideTutorial();

func TutorialCopleted(value):
	m_CurrentTutorialObject.UnrequisterTutorialCallback(self)
	m_CurrentTutorial = m_CurrentTutorial + 1;
	ExecuteNextTutorial();

func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");

#tasks

func AddNewTask(type):
	var taskType = TasksForThisLevel[type];
	var newTask = TaskPrefab.instance();
	var amountOfElements = int(rand_range(1,5));
	var RequirementsArray = Array();
	if GlobalLoader.GetMainProfile().Tutorial:
		RequirementsArray.append(DebrisForTask[type][0]);
	else:
		while amountOfElements >0:
			var debrisType = int(rand_range(0,DebrisForTask[type].size()))
			RequirementsArray.append(DebrisForTask[type][debrisType]);
			amountOfElements = amountOfElements -1;
	if taskType == TaskType.PointsTask:
		newTask.InitializeTask("points",RequirementsArray, self);
	elif taskType ==TaskType.ExplosiveTask:
		newTask.InitializeTask("explosive",RequirementsArray, self);
	elif taskType ==TaskType.TransmutatorTask:
		newTask.InitializeTask("trasmutator",RequirementsArray, self);
	elif taskType ==TaskType.MaterialTask:
		newTask.InitializeTask("materials",RequirementsArray, self);
	m_Tasks.append(newTask);
	HUD.RegisterTask(newTask);
	newTask.InitializePresentation();

func RenewTask(number):
	var task = m_Tasks[number];
	var amountOfElements = int(rand_range(1,5));
	var RequirementsArray = Array();
	while amountOfElements >0:
		var debrisType = int(rand_range(0,DebrisForTask[number].size()))
		RequirementsArray.append(DebrisForTask[number][debrisType]);
		amountOfElements = amountOfElements -1;
	task.InitializeTask(task.m_TaskName,RequirementsArray, self);
	task.InitializePresentation();

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if m_ConsumedFuel !=0:
		m_CurrentFuelLevel = m_CurrentFuelLevel - m_ConsumedFuel;
		HUD.SetFuelLevel(m_CurrentFuelLevel)
		m_ConsumedFuel = 0; 
	if m_ConsumedMaterial !=0:
		m_CurrentMaterialLevel = m_CurrentMaterialLevel - m_ConsumedMaterial;
		HUD.SetMaterialLevel(m_CurrentMaterialLevel);	
		m_ConsumedMaterial = 0;

func CalculateVictoryPoints()->int:
	if m_LevelPoints >= StarCondition3:
		return 3;
	elif m_LevelPoints >= StarCondition2:
		return 2;
	elif m_LevelPoints >= StarCondition1:
		return 1;
	else: 
		return 0;

func _unhandled_input(event):
	if event is InputEventJoypadButton:
		if m_CurrentLevelState == GameState.Initial:
			if event.button_index == JOY_XBOX_A and event.pressed :
				m_CurrentLevelState = GameState.Running;
				get_tree().set_pause(false);
		if event.button_index == 11 and event.pressed :
			if m_CurrentLevelState == GameState.Running:
				m_CurrentLevelState = GameState.Paused;
				HUD.ShowInGameMenu();
				get_tree().set_pause(true);
			elif m_CurrentLevelState == GameState.End:
				get_tree().set_pause(false);
				GlobalLoader.goto_scene("res://Interfaces/MissionsMap.tscn")
			else:
				StartLevel();

func ResumeGame():
	if m_CurrentLevelState == GameState.Paused:
		m_CurrentLevelState = GameState.Running;
		get_tree().set_pause(false);

func ConsumeFuel(value:float):
	m_ConsumedFuel = m_ConsumedFuel+ value;
	
func ConsumeMaterial(value:float):
	m_ConsumedMaterial = m_ConsumedMaterial + value;

func StartLevel():
	if m_CurrentLevelState == GameState.Initial:
		m_CurrentLevelState = GameState.Running;
		get_tree().set_pause(false);

#Drone method connect signal
func DroneConsumeFuelSignalEmited(value):
	ConsumeFuel(value)

func LevelFinished():
	m_CurrentLevelState = GameState.End
	get_tree().set_pause(true);
	var victoryPoints = CalculateVictoryPoints();
	HUD.ShowGameOver(victoryPoints)
	var profile = GlobalLoader.GetMainProfile();
	profile.FinishLevel(LevelNumber-1,victoryPoints);

#HUD signals
func _on_HUD_sig_FuelDepleted():
	LevelFinished()
	
func _on_HUD_sig_MaterialDepleted():
	LevelFinished()
	
#MotherShip Signals
func _on_MotherShip_MotherShipConsumeFuel(value):
	ConsumeFuel(value);

func _on_MotherShip_MotherShipConsumeMaterial(value):
	ConsumeMaterial(value)

func _on_MotherShip_MotherShipSpawnDrone(drone):
	drone.connect("DroneConsumeFuel",self,"DroneConsumeFuelSignalEmited")

func _on_MotherShip_MotherShipDockDrone(drone):
		pass

func _on_MotherShip_MotherShipConsumeDebris(debris):
	for task in m_Tasks:
		if task as CTask:
			if task.UpdateRequirements(debris):
				return;

func _on_TaskCompleted(taskCompleted):
	for number in range(0,m_Tasks.size()):
		var task = m_Tasks[number];
		if task as CTask:
			if task == taskCompleted:
				if task.m_TaskReward != null:
					if task.m_TaskReward.has_method("GetReward"):
						task.m_TaskReward.GetReward(self);
					else:
						m_LevelPoints = m_LevelPoints + task.m_TaskReward.m_Points;
						HUD.SetPoints(m_LevelPoints);
						print("add points")
				RenewTask(number);
				return;

func CallSpawnExplosives():
	if(m_MotherShip):
		if( m_MotherShip.has_method("SpawnExplosive")):
			m_MotherShip.SpawnExplosive();

func CallSpawnTrasmutator():
	if(m_MotherShip):
		if( m_MotherShip.has_method("SpawnTransmutator")):
			m_MotherShip.SpawnTransmutator();

func CallIncreaseMaterials():
	pass

func _on_MotherShip_RegisterMotherShip(ship):
	m_MotherShip = ship;


func _on_HUD_sig_ResumeGame():
	if m_CurrentLevelState == GameState.Paused:
		m_CurrentLevelState = GameState.Running;
		get_tree().set_pause(false);
