extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var mainProfile = GlobalLoader.GetMainProfile();
	var CollectedStars = mainProfile.CollectedStars;
	for a in range(0,$Levels.get_child_count()):
		var child = $Levels.get_child(a)
		if child as BaseButton:
			if mainProfile.Levels[a].StarsRequired <= CollectedStars :
				child.set_disabled(false);
				for stars in range (0,mainProfile.Levels[a].StarsCollected):
					var star = child.get_child(0).get_child(stars);
					if star:
						star.set_visible(true);
			else:
				child.set_disabled(true);
	$Levels/level1.grab_focus();
	$StarsLabel.set_text("stars Count: "+String(CollectedStars))
	$Tutorial.set_pressed(GlobalLoader.GetMainProfile().Tutorial);


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_level1_pressed():
	GlobalLoader.goto_scene("res://Scenes/level1.tscn")


func _on_level2_pressed():
	GlobalLoader.goto_scene("res://Scenes/level2.tscn")


func _on_level3_pressed():
	GlobalLoader.goto_scene("res://Scenes/level3.tscn")


func _on_Exit_pressed():
	GlobalLoader.goto_scene("res://Interfaces/MainMenu.tscn")


func _on_Tutorial_toggled(button_pressed):
	GlobalLoader.GetMainProfile().Tutorial = button_pressed;
