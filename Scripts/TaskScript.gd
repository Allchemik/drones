class_name CTask
extends Control

class RequirementPresentation:
	var m_TargetObject;
	var m_TargetPresentation;
	var m_Collected;

class PointsReward:
	var m_Points:int;

class ExplosiveReward:
	func GetReward(levelController):
		if levelController.has_method("CallSpawnExplosives"):
			levelController.CallSpawnExplosives();

class TransmutatorReward:
	func GetReward(levelController):
		if levelController.has_method("CallSpawnTrasmutator"):
			levelController.CallSpawnTrasmutator();

class MaterialReward:
	func GetReward(levelController):
		pass

var m_NextRequirement:RequirementPresentation;
var m_TaskName:String;
var m_TaskRequirements:Array;
var m_TaskRequirementsPresentation:Array;
var m_TaskReward = null

signal TaskCompletedSignal(task)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func InitializeTask(Name, Requirements, LevelController):
	if m_TaskName.empty():
		connect("TaskCompletedSignal",LevelController,"_on_TaskCompleted")
	m_TaskName = Name;
	if m_TaskName == "points":
		m_TaskReward = PointsReward.new();
		m_TaskReward.m_Points = 1;
	elif m_TaskName == "explosive":
		m_TaskReward = ExplosiveReward.new();
	elif m_TaskName == "trasmutator":
		m_TaskReward = TransmutatorReward.new();
	elif m_TaskName == "materials":
		m_TaskReward = MaterialReward.new();
	m_TaskRequirements = Requirements;
	m_TaskRequirementsPresentation.clear();


func GrantReward():
	for ob in m_TaskRequirementsPresentation:
		ob.m_TargetPresentation.queue_free();
	m_TaskRequirementsPresentation.clear();
	emit_signal("TaskCompletedSignal",self)

func UpdateRequirements(inputObject)->bool:
	if inputObject as CDebris:
		if inputObject.m_DebrisMaterialType == m_NextRequirement.m_TargetObject:
				m_NextRequirement.m_Collected = true;
				UpdateTaskState();
				return true;
	return false;

func InitializePresentation():
	for ob in m_TaskRequirements:
		var colorRect = ColorRect.new();
		colorRect.color =GlobalDataTypes.DebrisTypes[ob].m_color;
		colorRect.set_custom_minimum_size(Vector2(25,25));
		var rRepresentation = RequirementPresentation.new();
		rRepresentation.m_TargetObject = ob;
		rRepresentation.m_TargetPresentation = colorRect;
		rRepresentation.m_Collected = false;
		m_TaskRequirementsPresentation.append(rRepresentation);
		$RequirementsContainer.add_child(colorRect);
	m_NextRequirement = m_TaskRequirementsPresentation.front();
	$Label.text = m_TaskName;

func UpdateTaskState():
	for ob in m_TaskRequirementsPresentation:
		if ob as RequirementPresentation:
			if ob.m_Collected:
				if ob.m_TargetPresentation.is_visible():
					ob.m_TargetPresentation.set_visible(false);
			else:
				m_NextRequirement = ob;
				return;
	GrantReward();

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
