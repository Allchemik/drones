class_name CSmallAsteroid

extends Node2D
onready var debrisPrefab = preload("res://obejcts/Debris.tscn")

export(GlobalDataTypes.DebrisTypeName) var DebrisType =-1;
var m_childDebris;
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if DebrisType>=0:
		InitializeAsteroid(DebrisType)

func InitializeAsteroid(debrisType):
	var targetParent = get_parent().find_node("Debris");
	if targetParent:
		var newDebris = debrisPrefab.instance();
		targetParent.add_child(newDebris);
		newDebris.set_global_position(self.get_global_position())
		var newDebrisObject = newDebris.find_node("RigidBody2D");
		if newDebrisObject as CDebris:
			newDebrisObject.initializeDebrisOfType(debrisType);
		

func DestroyAsteroid(object):
	$Sprite.set_visible(false);
	$ExplosionParticle.set_emitting(true);
	$Timer.start();

func _on_Timer_timeout():
	queue_free();
