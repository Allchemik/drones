extends RigidBody2D

onready var dronePrefab = preload("res://obejcts/Drone.tscn")
onready var explosivePrefab = preload("res://obejcts/Explosive.tscn")
onready var transmutatorPrefab = preload("res://obejcts/Transmutator.tscn")

signal RegisterMotherShip(ship);

signal MotherShipConsumeFuel(value)
signal MotherShipConsumeMaterial(value)
signal MotherShipSpawnDrone(drone)
signal MotherShipDockDrone(value)

signal MotherShipSpawnExplosive()

signal MotherShipConsumeDebris(debris);

var m_droneHangars:Array = [false,false,false,false];
var m_droneColors:Array = [Color.red,Color.green,Color.blue,Color.purple];
var m_connectedJoysticks:Array;
var transparency =1;
export var DronePrice:float = 10;
export var ExplosivePrice:float = 5;
export var TransmutatorPrice:float = 10;
export var FuelConsumption:float = 0.001;


func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");
	
func RegisterDelayedTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"StartTutorial",[true]);

func UnregisterDelayedTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"StartTutorial");


# Called when the node enters the scene tree for the first time.
func _ready():
	emit_signal("RegisterMotherShip",self);

func _unhandled_input(event):
	if event is InputEventJoypadButton:
		var ConnectedJoystickID = event.get_device();
		if m_connectedJoysticks.find(ConnectedJoystickID)<0:
			if event.button_index == JOY_XBOX_A :#and event.pressed:
				SpawnDrone(ConnectedJoystickID)


func UseObject(drone):
	MotherShipDockDrone(drone);

func MotherShipDockDrone(drone):
	var dockingDroneId = m_connectedJoysticks.find(drone.ConnectedJoystickID);
	if (dockingDroneId >=0) :
		emit_signal("MotherShipDockDrone",drone)
		emit_signal("MotherShipConsumeMaterial",-DronePrice)
		m_droneHangars[drone.m_PlayerID] = false;
		m_connectedJoysticks.erase(drone.ConnectedJoystickID);
		drone.controlled = false;
		drone.remove_from_group("Drones");
		drone.queue_free();

func MotherShipDroneDestroyed(drone):
	var dockingDroneId = m_connectedJoysticks.find(drone.ConnectedJoystickID);
	if (dockingDroneId >=0) :
		emit_signal("MotherShipDockDrone",drone)
		m_droneHangars[drone.m_PlayerID] = false;
		m_connectedJoysticks.erase(drone.ConnectedJoystickID);
		drone.controlled = false;
		drone.queue_free();

func GetInputArenas()->Array:
	var inputArenas = [$InputArena0,$InputArena1,$InputArena2,$InputArena3];
	return inputArenas;

func _physics_process(delta):
	set_angular_velocity(0.1)
	emit_signal("MotherShipConsumeFuel",FuelConsumption);
#	if $ShipGraphics:
#		var canvas = $ShipGraphics.set_modulate(Color(1,1,1,transparency))
#	if(transparency <1):
#		transparency = transparency +0.05;
		
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func findFirstEmptyHangar() -> int :
	for i in range(0,4):
		if m_droneHangars[i]== false:
			return i;
	return -1;
		

func _on_DockArenaPlayer1_body_entered(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.EnteredActiveArea(self);

func _on_DockArenaPlayer2_body_entered(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.EnteredActiveArea(self);

func _on_DockArenaPlayer3_body_entered(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.EnteredActiveArea(self);

func _on_DockArenaPlayer4_body_entered(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.EnteredActiveArea(self);

func _on_DockArenaPlayer1_body_exited(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.LeaveActiveArea();

func _on_DockArenaPlayer2_body_exited(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.LeaveActiveArea();

func _on_DockArenaPlayer3_body_exited(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.LeaveActiveArea();

func _on_DockArenaPlayer4_body_exited(body):
	if body.is_in_group("Drones"):
		if body is CDrone:
			body.LeaveActiveArea();
	else:
		ConsumeDebris(body)

func _on_InputArena0_body_entered(body):
	ConsumeDebris(body)

func _on_InputArena1_body_entered(body):
	ConsumeDebris(body)

func _on_InputArena2_body_entered(body):
	ConsumeDebris(body)

func _on_InputArena3_body_entered(body):
	ConsumeDebris(body)
			
func ConsumeDebris(body):
	if body.is_in_group("Debris"):
		emit_signal("MotherShipConsumeDebris",body)
		if body.has_method("Destroy"):
			body.Destroy();

func SpawnDrone(ConnectedJoystickID):
	m_connectedJoysticks.append(ConnectedJoystickID)
	var playerId = findFirstEmptyHangar();
	m_droneHangars[playerId] = true;
	var object = find_node("DockArenaPlayer"+String(playerId+1));
	var startTransform = object.transform.origin;
	var newDrone = dronePrefab.instance();
	newDrone.transform.origin=(startTransform);
	GlobalLoader.current_scene.add_child(newDrone);
	newDrone.set_global_position(object.get_global_position())
	var newDroneObject = newDrone.find_node("RigidBody2D");
	if newDroneObject.has_method("apply_central_impulse"):
		newDroneObject.apply_central_impulse(Vector2(-30,-30).rotated((-90*(m_connectedJoysticks.size()-1))+transform.get_rotation()));
	if  newDroneObject as CDrone:
		newDroneObject.InitializeControl(ConnectedJoystickID,playerId,m_droneColors[playerId]);
		emit_signal("MotherShipConsumeMaterial",DronePrice)
		emit_signal("MotherShipSpawnDrone",newDroneObject)


func SpawnExplosive():
	var playerId = findFirstEmptyHangar();
	var object = find_node("DockArenaPlayer"+String(playerId+1));
	var startTransform = object.transform.origin;
	var newExplosive = explosivePrefab.instance();
	newExplosive.transform.origin=(startTransform);
	get_tree().get_root().add_child(newExplosive);
	newExplosive.set_global_position(object.get_global_position())
	var newExplosiveObject = newExplosive.find_node("RigidBody2D");
	if newExplosiveObject.has_method("apply_central_impulse"):
		newExplosiveObject.apply_central_impulse(Vector2(-30,-30).rotated((-90*(playerId))+transform.get_rotation()));
		emit_signal("MotherShipConsumeMaterial",ExplosivePrice)	
		emit_signal("MotherShipSpawnExplosive");

func SpawnTransmutator():
	var playerId = findFirstEmptyHangar();
	var object = find_node("DockArenaPlayer"+String(playerId+1));
	var startTransform = object.transform.origin;
	var newTransmutator = transmutatorPrefab.instance();
	newTransmutator.transform.origin=(startTransform);
	get_tree().get_root().add_child(newTransmutator);
	newTransmutator.set_global_position(object.get_global_position())
	if newTransmutator as CTransmutator:
		var newTransmutatorObject = newTransmutator.GetDissasembledBody();
		if newTransmutatorObject.has_method("apply_central_impulse"):
			newTransmutatorObject.apply_central_impulse(Vector2(-30,-30).rotated((-90*(playerId))+transform.get_rotation()));
			emit_signal("MotherShipConsumeMaterial",TransmutatorPrice)	

func _on_GameArea_body_exited(body):
		if body.is_in_group("Drones"):
			MotherShipDroneDestroyed(body);
