class_name CBaseObjectRigidBody2D

extends RigidBody2D

var m_MainObject:Node = null;

func _ready():
	m_MainObject=get_parent();

func GetObject()->Node:
	return m_MainObject;

func Pickup(picker:Node):
	if m_MainObject.has_method("Pickup"):
		m_MainObject.Pickup(picker);

func Drop(picker:Node):
	if m_MainObject.has_method("Drop"):
		m_MainObject.Drop(picker);
	
func UseObject(User:Node):
	if m_MainObject.has_method("UseObject"):
		m_MainObject.UseObject(User);

func RegisterTutorialCallback(tutorialObject, signalName):
	m_MainObject.RegisterTutorialCallback(tutorialObject,signalName);
	
func UnregisterTutorialCallback(tutorialObject, signalName):
	m_MainObject.UnregisterTutorialCallback(tutorialObject,signalName);

func Destroy():
	if m_MainObject.has_method("Destroy"):
		m_MainObject.Destroy();

func CanGrab()->bool:
	if m_MainObject.has_method("CanGrab"):
		return m_MainObject.CanGrab();
	else:
		return false
