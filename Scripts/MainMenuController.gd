extends Control

func _ready():
	var profil = GlobalLoader.GetMainProfile();
	if !profil.LoadProfile():
		profil.InitializeProfile();
		profil.SaveProfile();
		$ButtonsContainer/Continue.set_disabled(true);
	else:
		$ButtonsContainer/Continue.set_disabled(false);
	$ButtonsContainer/Play.grab_focus();

func _on_Play_pressed():
	var profil = GlobalLoader.GetMainProfile();
	profil.InitializeProfile();
	profil.SaveProfile();
	GlobalLoader.goto_scene("res://Interfaces/MissionsMap.tscn")

func _on_Exit_pressed():
	get_tree().quit();
