class_name CTutorial
extends Control

enum requestedActions {SpawnDrone,
MoveDrone,
TouchPickable,
PickObject,
ConsumeDebris,
PickExplosive,
AttachExplosive,
UseExplosive}

class CTutorialObject:
	var m_TutorialGUI:Node = null;
	var m_DelayStartTrigger:Node = null;
	var m_Target = null;
	var m_CircleTarget = null;
	var m_RequestedAction = null;
	var m_TargetCircleObjects:Array;

	signal TutorialCompleted(value);
	
	func CompleteTutorial(value):
		m_TutorialGUI.HideTutorial();
		if m_RequestedAction == requestedActions.SpawnDrone:
			m_Target.UnregisterTutorialCallback(self,"MotherShipSpawnDrone");
		elif m_RequestedAction == requestedActions.MoveDrone:
			GlobalLoader.current_scene.find_node(m_TutorialGUI.m_LevelController.get_name(m_TutorialGUI.m_LevelController.get_name_count()-1)).TutorialCopleted(self);
			m_Target.UnregisterTutorialCallback(self,"RequestedButtonPressed");
		elif m_RequestedAction == requestedActions.TouchPickable:
			for target in m_Target:
				target.UnregisterTutorialCallback(self,"DebrisInRange");
			m_Target.clear();
			for obj in m_TargetCircleObjects:
				obj.queue_free();
			m_TargetCircleObjects.clear();
		elif m_RequestedAction == requestedActions.PickObject:
			for target in m_Target:
				target.UnregisterTutorialCallback(self,"PickedUp");
			m_Target.clear();
			GlobalLoader.current_scene.find_node(m_TutorialGUI.m_LevelController.get_name(m_TutorialGUI.m_LevelController.get_name_count()-1)).TutorialCopleted(self);
		elif m_RequestedAction == requestedActions.ConsumeDebris:
			m_Target.UnregisterTutorialCallback(self,"MotherShipConsumeDebris");
			for obj in m_TargetCircleObjects:
				obj.queue_free();
			m_TargetCircleObjects.clear();
			var DebrisGenerator =  GlobalLoader.current_scene.find_node(m_TutorialGUI.m_DebrisGenerator.get_name(m_TutorialGUI.m_DebrisGenerator.get_name_count()-1));
			DebrisGenerator.m_running = true;
		elif m_RequestedAction == requestedActions.PickExplosive:
			if m_DelayStartTrigger != null:
				m_DelayStartTrigger.UnregisterDelayedTutorialCallback(self,"MotherShipSpawnExplosive");
			for target in m_Target:
				target.get_parent().UnregisterTutorialCallback(self,"PickedUp");
			m_Target.clear();
			for obj in m_TargetCircleObjects:
				obj.queue_free();
			m_TargetCircleObjects.clear();
		elif m_RequestedAction == requestedActions.AttachExplosive:
			for explosive in m_Target:
				explosive.get_parent().UnregisterTutorialCallback(self,"AsteroidInRange");
			for obj in m_TargetCircleObjects:
				obj.queue_free();
			m_TargetCircleObjects.clear();
		elif m_RequestedAction == requestedActions.UseExplosive:
			for explosive in m_Target:
				explosive.get_parent().UnregisterTutorialCallback(self,"Connecting");
				explosive.get_parent().UnregisterTutorialCallback(self,"Exploding");
			for obj in m_TargetCircleObjects:
				obj.queue_free();
			m_TargetCircleObjects.clear();
		emit_signal("TutorialCompleted",self);

	func StartTutorial(delayed:bool=false):
		m_TutorialGUI.ShowTutorial();
		if m_RequestedAction == requestedActions.SpawnDrone:
			m_Target.RegisterTutorialCallback(self,"MotherShipSpawnDrone")
			m_TutorialGUI.RequestButton(JOY_XBOX_A)
		elif m_RequestedAction == requestedActions.MoveDrone:
			m_Target=m_TutorialGUI;
			m_Target.RequestJoystick()
			m_Target.RegisterTutorialCallback(self,"RequestedButtonPressed");
		elif m_RequestedAction == requestedActions.TouchPickable:
			var circleObject = m_TutorialGUI.get_node("TargetTurorial/TextureRect").duplicate();
			m_TargetCircleObjects.append(circleObject);
			circleObject.get_node("TargetAnimation").play("RotateTarget");
			m_CircleTarget.add_child(circleObject);
			for drone in m_Target:
				drone.RegisterTutorialCallback(self,"DebrisInRange");
		elif m_RequestedAction == requestedActions.PickObject:
			m_TutorialGUI.RequestButton(JOY_XBOX_A);
			for object in m_Target:
				object.RegisterTutorialCallback(self,"PickedUp");
		elif m_RequestedAction == requestedActions.ConsumeDebris:
			m_Target.RegisterTutorialCallback(self,"MotherShipConsumeDebris");
			var tergets = m_Target.GetInputArenas();
			for target in tergets:
				var circleObject = m_TutorialGUI.get_node("TargetTurorial/TextureRect").duplicate();
				circleObject.get_node("TargetAnimation").play("RotateTarget");
				target.add_child(circleObject);
				m_TargetCircleObjects.append(circleObject);
		elif m_RequestedAction == requestedActions.PickExplosive:
			if m_DelayStartTrigger != null:
				if(!delayed):
					m_DelayStartTrigger.RegisterDelayedTutorialCallback(self,"MotherShipSpawnExplosive");
					return;
			m_Target = m_TutorialGUI.get_tree().get_nodes_in_group("Explosives");
			for explosive in m_Target:
				explosive.get_parent().RegisterTutorialCallback(self,"PickedUp");
				var circleObject = m_TutorialGUI.get_node("TargetTurorial/TextureRect").duplicate();
				m_TargetCircleObjects.append(circleObject);
				circleObject.get_node("TargetAnimation").play("RotateTarget");
				explosive.add_child(circleObject);
		elif m_RequestedAction == requestedActions.AttachExplosive:
			for explosive in m_Target:
				explosive.get_parent().RegisterTutorialCallback(self,"AsteroidInRange");
			for asteroid in m_CircleTarget:	
				var circleObject = m_TutorialGUI.get_node("TargetTurorial/TextureRect").duplicate();
				m_TargetCircleObjects.append(circleObject);
				circleObject.get_node("TargetAnimation").play("RotateTarget");
				asteroid.add_child(circleObject);
		elif m_RequestedAction == requestedActions.UseExplosive:
			m_TutorialGUI.RequestButton(JOY_XBOX_B);
			for explosive in m_Target:
				explosive.get_parent().RegisterTutorialCallback(self,"Connecting");
				explosive.get_parent().RegisterTutorialCallback(self,"Exploding");
				var circleObject = m_TutorialGUI.get_node("TargetTurorial/TextureRect").duplicate();
				m_TargetCircleObjects.append(circleObject);
				circleObject.get_node("TargetAnimation").play("RotateTarget");
				explosive.add_child(circleObject);


	func RequisterTutorialCallback(levelController):
		connect("TutorialCompleted",levelController,"TutorialCopleted");

	func UnrequisterTutorialCallback(levelController):
		disconnect("TutorialCompleted",levelController,"TutorialCopleted");


var m_ActiveCTutorialObject:CTutorialObject;
var m_Tutorials:Array;

var requestedButtonIndex = -1;
var requestedJoystickIndex = -1;

export(NodePath) var m_MotherShip;
export(NodePath) var m_DebrisGenerator;
export(NodePath) var m_LevelController;


signal RequestedButtonPressed;

func SpawnDroneTutorial()->CTutorialObject:
	var DebrisGenerator =  GlobalLoader.current_scene.find_node(m_DebrisGenerator.get_name(m_DebrisGenerator.get_name_count()-1));
	DebrisGenerator.m_running = false;
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_Target = GlobalLoader.current_scene.find_node(m_MotherShip.get_name(m_MotherShip.get_name_count()-1));
	newTutorialObject.m_RequestedAction =requestedActions.SpawnDrone;
	newTutorialObject.m_TutorialGUI	=self;
	return newTutorialObject;

func MoveDroneTutorial()->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_Target =  GlobalLoader.current_scene.find_node(m_LevelController.get_name(m_LevelController.get_name_count()-1));
	newTutorialObject.m_RequestedAction =requestedActions.MoveDrone;
	newTutorialObject.m_TutorialGUI	=self;
	return newTutorialObject;

func TouchPickableTutorial()->CTutorialObject:
	var DebrisGenerator =  GlobalLoader.current_scene.find_node(m_DebrisGenerator.get_name(m_DebrisGenerator.get_name_count()-1));
	var levelController =  GlobalLoader.current_scene.find_node(m_LevelController.get_name(m_LevelController.get_name_count()-1));
	var type = levelController.m_Tasks[0].m_TaskRequirements[0];
	var debris = DebrisGenerator.spawnTutorialDebris(Vector2(250,250),type);
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_CircleTarget = debris.find_node("RigidBody2D");
	newTutorialObject.m_RequestedAction =requestedActions.TouchPickable;
	newTutorialObject.m_TutorialGUI	=self
	newTutorialObject.m_Target = get_tree().get_nodes_in_group("Drones");
	return newTutorialObject;

func PickDebrisTutorial(debris)->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_RequestedAction =requestedActions.PickObject;
	newTutorialObject.m_TutorialGUI	=self
	newTutorialObject.m_Target = get_tree().get_nodes_in_group("Debris");
	return newTutorialObject;

func ConsumeDebrisTutorial()->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_Target = GlobalLoader.current_scene.find_node(m_MotherShip.get_name(m_MotherShip.get_name_count()-1));
	newTutorialObject.m_RequestedAction =requestedActions.ConsumeDebris;
	newTutorialObject.m_TutorialGUI	=self
	return newTutorialObject;

func PickExplosiveTutorial()->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_DelayStartTrigger= GlobalLoader.current_scene.find_node(m_MotherShip.get_name(m_MotherShip.get_name_count()-1));
	newTutorialObject.m_RequestedAction =requestedActions.PickExplosive;
	newTutorialObject.m_TutorialGUI	=self
	newTutorialObject.m_Target = get_tree().get_nodes_in_group("Explosives");
	return newTutorialObject;

func AttachExplosiveTutorial()->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()	
	newTutorialObject.m_RequestedAction =requestedActions.AttachExplosive;
	newTutorialObject.m_TutorialGUI	=self
	newTutorialObject.m_Target = get_tree().get_nodes_in_group("Explosives");
	newTutorialObject.m_CircleTarget = get_tree().get_nodes_in_group("Asteroids");
	return newTutorialObject;

func UseExplosiveTutorial()->CTutorialObject:
	var newTutorialObject = CTutorialObject.new()
	newTutorialObject.m_RequestedAction =requestedActions.UseExplosive;
	newTutorialObject.m_TutorialGUI	=self
	newTutorialObject.m_Target = get_tree().get_nodes_in_group("Explosives");
	return newTutorialObject;

func MakeToutorialObject(TutorialType,object=null)->CTutorialObject:
	if TutorialType == requestedActions.SpawnDrone:
		return SpawnDroneTutorial();
	elif TutorialType == requestedActions.MoveDrone:
		return MoveDroneTutorial();
	elif TutorialType == requestedActions.TouchPickable:
		return TouchPickableTutorial();
	elif TutorialType == requestedActions.PickObject:
		return PickDebrisTutorial(object);
	elif TutorialType == requestedActions.ConsumeDebris:
		return ConsumeDebrisTutorial();
	elif TutorialType == requestedActions.PickExplosive:
		return PickExplosiveTutorial();
	elif TutorialType == requestedActions.AttachExplosive:
		return AttachExplosiveTutorial();
	elif TutorialType == requestedActions.UseExplosive:
		return UseExplosiveTutorial();
	else:
		return CTutorialObject.new();
		

func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");

func _ready():
	pass
	#HideTutorial();

func ShowTutorial():
	set_visible(true);

func HideTutorial():
	set_visible(false);

func ShowGamepadTutorial():
	$GamePadTutorial.set_visible(true)

func HideGamepadTutorial():
	$GamePadTutorial.set_visible(false)

func RequestButton(buttonIndex):
	requestedButtonIndex = buttonIndex;
	if buttonIndex == JOY_XBOX_A:
		ShowBottomButton();
	if buttonIndex == JOY_XBOX_B:
		ShowRightButton();
	if buttonIndex == JOY_XBOX_Y:
		ShowTopButton();
	if buttonIndex == JOY_XBOX_X:
		ShowLeftButton();
	if buttonIndex == JOY_START :
		ShowStartButton();
	if buttonIndex == JOY_L:
		ShowLeftBumper();
	if buttonIndex == JOY_R:
		ShowRightBumper();

func RequestJoystick():
	requestedJoystickIndex = 0;
	ShowJoystick();

func ShowBottomButton():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("bottomButtonClick");

func ShowTopButton():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("topButtonClick");

func ShowLeftButton():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("leftButtonClick");

func ShowRightButton():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("rightButtonClick");

func ShowStartButton():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("startButtonClick");

func ShowLeftBumper():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("leftBumperClick");

func ShowRightBumper():
	ShowGamepadTutorial();
	$GamePadTutorial/ButtonsAnimation.play("rightBumperClick");

func HideButton():
	$GamePadTutorial/ButtonsAnimation.stop();
	$GamePadTutorial/ButtonsAnimation.seek(0,true);
	requestedButtonIndex = -1;

func ShowJoystick():
	ShowGamepadTutorial();
	$GamePadTutorial/JoystickAnimation.play("JoystickClick");
	
func HideJoystick():
	$GamePadTutorial/JoystickAnimation.seek(0,true);
	$GamePadTutorial/JoystickAnimation.stop(true)
	requestedJoystickIndex = -1;

func _unhandled_input(event):
	if event is InputEventJoypadButton:
		if event.button_index == requestedButtonIndex and event.pressed == true:
			HideButton();
			emit_signal("RequestedButtonPressed",self)
	if event is InputEventJoypadMotion:
		if (requestedJoystickIndex == 0 or requestedJoystickIndex == 1):
			if event.axis == 0 or event.axis == 1:
				HideJoystick();
				emit_signal("RequestedButtonPressed",self)
				
	if requestedButtonIndex == -1 and requestedJoystickIndex == -1 :
		HideGamepadTutorial();


func ShowTutorialTargetCircle():
	pass
	
func attachCircleToTarget(target):
	pass
	
func clearTargetCircleFromTarget(target):
	pass
