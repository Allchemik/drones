class_name CDrone

extends RigidBody2D

#signals 
signal DroneConsumeFuel(value)
signal DebrisInRange(value);
signal ExplosiveInRange(value);
signal TransmutatorInRange(value);

var controlled: bool = false;
var ConnectedJoystickID = -1;
var m_PlayerID = -1;
export var speed:int = 10;
export var JumpForce:int = 10;
export var torqueSpeed:int = 5;
export var FuelConsumption:float = 0.001;

var torque:float =0.0;
var x:float = 0.0;
var y:float = 0.0;


# emmiter engines
export (NodePath) var leftEngineNode;
var leftEngine;
export (NodePath) var rightEngineNode;
var rightEngine;
export (NodePath) var topEngineNode;
var topEngine;
export (NodePath) var bottomEngineNode;
var bottomEngine;
export (NodePath) var platformNode;
var platform;


var forceBreaking:bool = false;
var canGrab: bool = false;
var isGrabing: bool = false;
var canUse:bool = false;

var heldObject: Node = null;
var objectInRage : Node = null;
var grabOffset: Vector2 = Vector2.ZERO;
var grabRotationOffset : float = 0;
var grabRotationInitialalue : float =0;
var stepHolderVector:Vector2 =  Vector2.ZERO;
var stepHolderAngle: float =0.0;

var m_ActiveAreaInRange:Node = null;

# Called when the node enters the scene tree for the first time.

func _ready():
	leftEngine = get_node(leftEngineNode)
	rightEngine = get_node(rightEngineNode)
	topEngine = get_node(topEngineNode)
	bottomEngine = get_node(bottomEngineNode)
	platform = get_node(platformNode)
	$HoldPosition/HoldJoint.node_a = self.get_path();
	$HoldPosition/CollisionShape2D/Polygon2D2.color = Color.white;
	

func _unhandled_input(event):
	#if !controlled:
	#	if event is InputEventJoypadButton:
#			if event.button_index == JOY_XBOX_A and event.pressed:
#				controlled = true;
#				ConnectedJoystickID = event.get_device();
	if controlled:
		if event.get_device() != ConnectedJoystickID:
			return;
		if event is InputEventJoypadMotion:
			if event.get_axis() == JOY_AXIS_0:
				x = event.get_axis_value();
			if event.get_axis() == JOY_AXIS_1:
				y = event.get_axis_value();
		if event is InputEventJoypadButton:
			if event.pressed == true:
				if event.button_index == JOY_L:
					torque = torque - (5*torqueSpeed);
				if event.button_index == JOY_R:
					torque = torque + (5*torqueSpeed);
				if event.button_index == JOY_XBOX_X:
					forceBreaking = true;
				if event.button_index == JOY_XBOX_Y:
					var x = Input.get_joy_axis(ConnectedJoystickID,JOY_ANALOG_LX)*speed*JumpForce
					var y = Input.get_joy_axis(ConnectedJoystickID,JOY_ANALOG_LY)*speed*JumpForce
					apply_central_impulse(Vector2(x,y))
					emit_signal("DroneConsumeFuel",1);
				if event.button_index == JOY_XBOX_A:
					if !isGrabing:
						PickUp();
					else:
						Drop();
				if event.button_index == JOY_XBOX_B:
					if m_ActiveAreaInRange != null:
						if m_ActiveAreaInRange.has_method("UseObject"):
							m_ActiveAreaInRange.UseObject(self);
					elif canUse:
						if objectInRage != null:
							if objectInRage.has_method("UseObject"):
								objectInRage.UseObject(self);							
			else:
				if event.button_index == JOY_L:
					torque = torque + (5*torqueSpeed);
				if event.button_index == JOY_R:
					torque = torque - (5*torqueSpeed);
				if event.button_index == JOY_XBOX_X:
					forceBreaking = false;
						


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if controlled:
		if forceBreaking:
			apply_central_impulse(-get_linear_velocity()*0.05);
			apply_torque_impulse(-get_angular_velocity()*0.05);
			emit_signal("DroneConsumeFuel",FuelConsumption)
		platform.set_global_position(self.get_global_position());

func _physics_process(delta):
	if controlled:
		if sign(x)>0:
			leftEngine.set_emitting(false)
			rightEngine.set_emitting(true)
		elif sign(x)<0:
			leftEngine.set_emitting(true)
			rightEngine.set_emitting(false)
		else:
			leftEngine.set_emitting(false)
			rightEngine.set_emitting(false)
		if sign(y)>0:
			bottomEngine.set_emitting(false)
			topEngine.set_emitting(true)
		elif sign(y)<0:
			bottomEngine.set_emitting(true)
			topEngine.set_emitting(false)
		else:
			bottomEngine.set_emitting(false)
			topEngine.set_emitting(false)
		if abs(x)>0 or abs(y)>0:
			apply_central_impulse(Vector2(x*speed*delta, y*speed*delta))
			emit_signal("DroneConsumeFuel",FuelConsumption)
		if abs(torque)>0:
			apply_torque_impulse(torque)

func InitializeControl(joystickID, PlayerID, color):
	if !controlled :
		ConnectedJoystickID = joystickID;
		controlled = true;
		m_PlayerID = PlayerID;
		$Polygon2D.set_color(color);

func EnteredActiveArea(activeArea):
	m_ActiveAreaInRange = activeArea;

func LeaveActiveArea():
	m_ActiveAreaInRange = null;

func Drop():
	if heldObject == null :
		return;
	$HoldPosition/HoldJoint.node_b = "";
	#if heldObject.has_method("drop"):
	#		heldObject.drop(self)
	if heldObject.has_method("Drop"):
		heldObject.Drop(self)
	isGrabing = false;
	heldObject = null;
	canGrab = false;
	$HoldPosition/CollisionShape2D/Polygon2D2.color = Color.white;
	print("drop")
	
func PickUp():
	if canGrab and objectInRage != null:
		isGrabing = true;
		heldObject = objectInRage;
		if heldObject.has_method("Pickup"):
			heldObject.Pickup(self)
		$HoldPosition/HoldJoint.node_b = heldObject.get_path();
		$HoldPosition/CollisionShape2D/Polygon2D2.color = Color.black;
		print("pick up")

func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");

func _on_Area2D_body_entered(body):
	if body.has_method("CanGrab"):
		canGrab = body.CanGrab();
	else:
		canGrab = false;
	if body.is_in_group("Debris"):
		canUse = false;
		objectInRage = body;
		emit_signal("DebrisInRange",self)
	elif body.is_in_group("Explosives"):
		canUse = true;
		objectInRage = body;
		emit_signal("ExplosiveInRange",self)
	elif body.is_in_group("Transmutator"):
		canUse = true;
		objectInRage = body;
		emit_signal("TransmutatorInRange",self);
	
func _on_Area2D_body_exiFted(body):
	if body.is_in_group("Debris") or body.is_in_group("Explosives") or body.is_in_group("Transmutator"):
		canGrab = false;
		canUse = false;
		objectInRage = null;

