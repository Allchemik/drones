extends Node2D

class_name CDebris
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var m_DebrisMaterialType =-1;
var original_object_parent:Node;
var linearVelocity:Vector2= Vector2.ZERO;
var freeFloatVelocity:Vector2 = Vector2.ZERO;
var angularVelocity:float= 0.0;
var m_floatingFreely:bool = false;
onready var lastRotation = get_global_rotation();
onready var lastPosition = get_global_position();
var m_picker:Node = null;
# Called when the node enters the scene tree for the first time.

signal PickedUp(value)

signal Dissapearing();

func RegisterTutorialCallback(tutorialObject, signalName):
	connect(signalName, tutorialObject,"CompleteTutorial");

func UnregisterTutorialCallback(tutorialObject, signalName):
	disconnect(signalName, tutorialObject,"CompleteTutorial");

func CanGrab()->bool:
	return true;

func Pickup (picker:Node):
	m_floatingFreely = false;
	if(m_picker!= null):
		return;
	emit_signal("PickedUp",self);
	m_picker = picker;
	if(m_picker.has_method("Drop")):
		connect("Dissapearing",m_picker,"Drop");

func Drop(value):
	if m_picker!=null:
		if is_connected("Dissapearing",m_picker,"Drop"):
			disconnect("Dissapearing",m_picker,"Drop");
			m_picker=null;
	
func initializeDebrisOfType(type):
	m_DebrisMaterialType = type;
	$CollisionPolygon2D/Polygon2D.set_color(GlobalDataTypes.DebrisTypes[type].m_color)

func _physics_process(delta):
	if(m_floatingFreely):
		$RigidBody2D.set_linear_velocity(freeFloatVelocity);
	else:
		linearVelocity = ($RigidBody2D.get_global_position()-lastPosition)/delta;
		angularVelocity = ($RigidBody2D.get_global_rotation()-lastRotation)/delta;
		lastPosition = $RigidBody2D.get_global_position();
		lastRotation = $RigidBody2D.get_global_rotation();

func InitializeFreeFloat(velocity :Vector2):
	m_floatingFreely = true;
	freeFloatVelocity = velocity;

func Destroy():
	emit_signal("Dissapearing");
	queue_free();
	
